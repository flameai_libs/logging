from setuptools import setup


setup(
    name="common_logging",
    description="My customized library for logging in projects",
    version="v0.1.1",
    author="Alexander Andryukov",
    author_email="andryukov@gmail.com",
    extras_require={
        "dev": ["bump-my-version==0.20.0", "ruff==0.3.5"],
        "tests": ["pytest==8.1.1"],
    },
)
